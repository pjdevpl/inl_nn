from vectorize import *


class CorpusParser:
    def __init__(self, file):
        self.file = file
        self.vectorizer = None

    def parse_corpus(self):
        f = open(self.file, 'r', encoding="utf-8")
        loaded_lines = []
        for line in f:
            if not line.startswith("%%"):
                splitted_line = line.split("\t")
                loaded_lines.append(AnnotatedWord(word=splitted_line[0], pos=splitted_line[2].split(":")[0]))
        return loaded_lines

    def vectorize_corpus(self):
        parsed_corpus = self.parse_corpus()
        labels = list(set([w.pos for w in parsed_corpus]))
        self.vectorizer = Vectorizer(labels)
        return [vaw for vaw in [self.vectorizer.annotated_word_to_vec(aw) for aw in parsed_corpus] if vaw is not None]
