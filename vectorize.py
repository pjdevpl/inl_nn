from gensim.models.word2vec import *
from sklearn.preprocessing import LabelBinarizer
from model import *


class Vectorizer:

    def __init__(self, labels):
        self.model = Word2Vec.load('vectors/nkjp+wiki-forms-all-100-skipg-ns')
        print('Word2Vec model loaded')
        self.labels = labels
        self.encoder = LabelBinarizer()
        self.encoder.fit_transform(self.labels)

    def word2vec(self, word):
        return self.model[word]

    def label2vec(self, label):
        return self.encoder.transform([label])[0]

    def annotated_word_to_vec(self, annotated_word: AnnotatedWord):
        try:
            return VectorizedAnnotatedWord(self.word2vec(annotated_word.word), self.label2vec(annotated_word.pos))
        except KeyError:
            return None
