class AnnotatedWord:
    def __init__(self, word, pos):
        self.word = word
        self.pos = pos


class VectorizedAnnotatedWord:
    def __init__(self, v_word, v_pos):
        self.v_word = v_word
        self.v_pos = v_pos
