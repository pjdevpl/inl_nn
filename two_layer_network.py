from keras.layers import Dense
from keras.layers import LSTM
from keras.models import Sequential
from matplotlib import pyplot
import numpy as np


class TwoLayerNetwork:
    def __init__(self, all_data, labels):
        self.all_data = all_data
        self.labels = labels

        self.model_p1 = Sequential()
        self.history = None

    def train(self):
        all_count = len(self.all_data)
        train = self.all_data[0:int(all_count * 0.80)]
        val = self.all_data[int(all_count * 0.80):int(all_count * 0.90)]

        train_inp = np.asarray([vaw.v_word for vaw in train])
        train_out = np.asarray([vaw.v_pos for vaw in train])
        val_inp = np.asarray([vaw.v_word for vaw in val])
        val_out = np.asarray([vaw.v_pos for vaw in val])

        self.model_p1.add(Dense(100, activation='sigmoid'))
        self.model_p1.add(Dense(len(self.labels), activation='sigmoid'))
        self.model_p1.compile(loss='categorical_crossentropy', optimizer='adam', metrics=['accuracy'])

        self.history = self.model_p1.fit(train_inp, train_out, epochs=50, verbose=1, validation_data=(val_inp, val_out))
        return self.history.history["val_acc"][-1]

    def plot(self):
        pyplot.plot(self.history.history['acc'][0:])
        pyplot.plot(self.history.history['val_acc'][0:])
        pyplot.xlabel('epoch')
        pyplot.ylabel('acc')
        pyplot.legend(['train', 'val'])
        pyplot.title("Two-Layer")
        pyplot.show()

    def evaluate(self):
        all_count = len(self.all_data)
        test = self.all_data[int(all_count * 0.90):all_count]
        test_inp = np.asarray([vaw.v_word for vaw in test])
        test_out = np.asarray([vaw.v_pos for vaw in test])
        x = self.model_p1.evaluate(test_inp, test_out, verbose=1)
        return x

