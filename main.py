from parse import CorpusParser
from ltsm_network import LstmNetwork
from two_layer_network import TwoLayerNetwork


p = CorpusParser("corpus/nkjp_tab.txt")
data_all = p.vectorize_corpus()

lstm = LstmNetwork(data_all, p.vectorizer.labels)
lstm_train_result = lstm.train()

two_layer = TwoLayerNetwork(data_all, p.vectorizer.labels)
two_layer_train_result = two_layer.train()

if lstm_train_result > two_layer_train_result:
    print("Picking up LSTM")
    acc = lstm.evaluate()[1]
    print("Accuracy for test data: ", acc)
else:
    print("Picking up Two Layer Network")
    acc = two_layer.evaluate()[1]
    print("Accuracy for test data: ", acc)

lstm.plot()
two_layer.plot()

